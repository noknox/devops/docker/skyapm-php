FROM golang:1.13.8-alpine3.11 as skywalking-build

RUN wget https://github.com/SkyAPM/SkyAPM-php-sdk/archive/v3.3.2.tar.gz -O /tmp/SkyAPM-php-sdk-v3.3.2.tar.gz && \
    tar -xzf /tmp/SkyAPM-php-sdk-v3.3.2.tar.gz -C /tmp && \
    cd /tmp/SkyAPM-php-sdk-3.3.2 && \
    go build -o /usr/local/bin/sky-php-agent ./cmd/main.go && \
    chmod +x /usr/local/bin/sky-php-agent

FROM alpine:3.11 as runtime

COPY --from=skywalking-build /usr/local/bin/sky-php-agent /usr/local/bin/sky-php-agent

RUN adduser --disabled-password www-data && \
    chown www-data /var/run

USER www-data
CMD sky-php-agent --grpc $SW_AGENT_COLLECTOR_BACKEND_SERVICES --sky-version=$SW_VERSION --socket $SW_SOCKET